import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_deno/Utils/Utils.dart';
import 'package:provider_deno/models/Book.dart';
import 'package:provider_deno/models/Books.dart';
import 'package:provider_deno/views/AddBookScreen.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Book> bookData = Utils().GetdataSimple();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<Books>(
      create: (context) => new Books(),
      child: Scaffold(
        appBar: AppBar(
          title: Text("Home Screen"),
          actions: [
            Consumer<Books>(builder: (context, mymodel, child) {
              return IconButton(
                  icon: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                  onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => new AddBookScreen(
                                books: mymodel,
                              ))));
            }),
          ],
        ),
        body: SafeArea(child: Container(
          child: Consumer<Books>(builder: (context, mymodel, child) {
            return _bookListView(context, mymodel);
          }),
        )),
      ),
    );
  }
}

Widget _bookListView(BuildContext context, Books books) {
  return ListView.builder(
    itemCount: books.getTotalCount,
    itemBuilder: (context, index) {
      return GestureDetector(
          child: Container(
              child: _itemBooksContentView(context, books.getData[index])),
          onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        new AddBookScreen.update(books, index)),
              ));
    },
  );
}

Widget _itemBooksContentView(BuildContext context, Book book) {
  return Container(
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5)),
          boxShadow: [
            BoxShadow(
              color: Colors.red.withOpacity(0.5),
              spreadRadius: 0.1,
              blurRadius: 20,
              offset: Offset(0, 0),
            )
          ]),
      child: Column(
        children: [
          Text(
            '${book.name}',
            style: TextStyle(fontSize: 30),
          ),
          Text('${book.type}')
        ],
      ));
}
