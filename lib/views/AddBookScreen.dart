import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_deno/models/Book.dart';
import 'package:provider_deno/models/Books.dart';

class AddBookScreen extends StatefulWidget {
  final Books books;
  int _index = -1;
  AddBookScreen({this.books});
  AddBookScreen.update(this.books, this._index);
  @override
  _AddBookScreenState createState() => _AddBookScreenState();
}

class _AddBookScreenState extends State<AddBookScreen> {
  bool _enableSave = false;
  TextEditingController _inputNameController = new TextEditingController();
  TextEditingController _inputTypeController = new TextEditingController();

  void _updateSaveStatus(bool status) {
    setState(() {
      _enableSave = status;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget._index < 0 ? 'Add Book Screen' : 'Update Book'),
        actions: [
          IconButton(
              icon: Icon(
                Icons.save,
                color: _enableSave ? Colors.red : Colors.grey,
              ),
              onPressed: _enableSave
                  ? () {
                      if (widget._index < 0) {
                        widget.books.addBook(Book(_inputNameController.text,
                            _inputTypeController.text));
                      } else {
                        widget.books.updateBook(
                            Book(_inputNameController.text,
                                _inputTypeController.text),
                            widget._index);
                      }
                      Navigator.pop(context);
                    }
                  : null)
        ],
      ),
      body: SafeArea(
          child: Container(
        child: Column(
          children: [
            _inputForm(
                context,
                widget._index < 0
                    ? "Input Name of book"
                    : widget.books.getItem(widget._index).name,
                _inputNameController),
            _inputForm(
                context,
                widget._index < 0
                    ? "Input Type of book"
                    : widget.books.getItem(widget._index).type,
                _inputTypeController),
          ],
        ),
      )),
    );
  }

  @override
  void initState() {
    _inputNameController.addListener(() {
      checkSaveStatus();
    });
    _inputTypeController.addListener(() {
      checkSaveStatus();
    });
    if (widget._index >= 0) {
      _inputNameController.text = widget.books.getItem(widget._index).name;
      _inputTypeController.text = widget.books.getItem(widget._index).type;
    }

    super.initState();
  }

  void checkSaveStatus() {
    if ((_inputNameController.text.isEmpty ||
            _inputTypeController.text.isEmpty) &&
        _enableSave) {
      _updateSaveStatus(false);
    } else if (_inputNameController.text.isNotEmpty &&
        _inputTypeController.text.isNotEmpty &&
        !_enableSave) {
      _updateSaveStatus(true);
    }
  }

  @override
  void dispose() {
    _inputNameController.dispose();
    _inputTypeController.dispose();
    super.dispose();
  }
}

Widget _inputForm(BuildContext context, String hint,
    TextEditingController textEditingController) {
  return Container(
    margin: EdgeInsets.all(10),
    child: TextField(
      controller: textEditingController,
      textAlign: TextAlign.left,
      decoration: InputDecoration(
          filled: true,
          hintText: hint,
          hintStyle: TextStyle(color: Colors.grey),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
          )),
    ),
  );
}
