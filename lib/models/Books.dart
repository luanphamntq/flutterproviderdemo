import 'package:flutter/material.dart';
import 'package:provider_deno/models/Book.dart';

class Books extends ChangeNotifier {
  final List<Book> _data = <Book>[];

  List<Book> get getData => _data;

  Book getItem(int index) => _data[index];

  int get getTotalCount => _data.length;

  void addBook(Book book) {
    _data.add(book);
    notifyListeners();
  }

  void updateBook(Book book, int index) {
    _data[index].name = book.name;
    _data[index].type = book.type;
    notifyListeners();
  }
}
