import 'package:provider_deno/models/Book.dart';

class Utils {
  List<Book> GetdataSimple() {
    List<Book> data = <Book>[];
    for (var i = 0; i < 3; i++) {
      data.add(new Book('Book ${i}', 'Type ${i}'));
    }
    return data;
  }
}
